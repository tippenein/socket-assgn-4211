#!/usr/bin/env python

# ====ATM====
# setup:
# ./atm atmID bank_name 

# bank_name is the host name of bank and it defaults a bind on port 8000
# if it gets a validate or check balance request it just passes it straight on

import os
import time
import socket
import select
import simplejson as json
from atm_utils import *

BUFS = 4092
MAX_CLIENTS = 3

class Atm(object):
  ''' atm object for providing a middleman between client and bank '''

  def __init__(self, atmID, bank_name, lowT=50000, highT=200000):
    ''' 
    @param atmID - specify the atm's identification name (3 digit alphanumeric)
    @param bank_name - the host name of bank / defaults a bind on port 8000
    Type ./atm.py --list to list the available atm IDs
    '''

    self.atmID        = atmID
    self.bank_name    = bank_name
    self.low_thresh   = lowT
    self.high_thresh  = highT
    self.bank_sock    = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.atm_sock     = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.average_stash= (lowT + highT) / 2.0
 
  def bind_socks(self):
    tup = get_atm_tuple(self.atmID)
    self.chost = tup[0]
    self.cport = int(tup[1])
    self.atm_sock.bind((self.chost, self.cport))
    self.atm_sock.listen(MAX_CLIENTS)
    self.atm_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.attempt_bank_conn()

  def serve(self):
    print "atm {} has a stash of: {}".format(self.atmID,self.get_available_cash())
    self.bind_socks()
    self.readlist = [ self.atm_sock, sys.stdin ]

    print 'listening for client connects on {}:{}'.format(self.chost, self.cport)
    quit = False
    while not quit:
      sread, swrite, sexc = select.select(self.readlist, [], [])
      for s in sread:
        # CLIENT OPERATIONS
        if s is self.atm_sock:
          client, addr = s.accept()
          client.setblocking(0)
          self.readlist.append(client)
          print "new client connected, awaiting requests"
        # if cli input            
        elif s == sys.stdin:
          admin_input = sys.stdin.readline()
          if admin_input.startswith('q'):
            quit = True
            break 
        else:
          # client request
          print "reading client's request"
          try:
            req = s.recv(BUFS)
            if req:
              d = json.loads(req)
              if d['type'] == "x":
                self.readlist.remove(s)
                s.close()
              else: self.process_transaction(req, s)
            else:
              print "client hung up"
              if s in self.readlist:
                self.readlist.remove(s)
                s.close()
          except socket.error:
            self.readlist.remove(s)
            print "client hung up"
            

    # end while
    self.shutdown()
  
  def process_transaction(self, j, conn):
    d = json.loads(j)
    _type   = d['type']
    acc     = d['acc']
    atmID   = self.atmID
    stash   = self.get_available_cash()
    try:
      passwd= d['passwd']
      amt   = d['amt']
      amt = float(amt)
    except:
      pass
    #Validate
    if d['type'] == 'v':
      print "processing validation"
      self.bank_sock.send(j)
      res = self.bank_sock.recv(BUFS)
      print "banks response: ".format(res)
      if res.startswith('validated'): 
        print "got validate request from {} with pass: {}".format(acc,passwd)
        conn.send('validated')
      else:
        conn.send('!valid')
    #Check 
    elif _type == 'cb':
      print "got check balance request from {}".format(acc)
      self.bank_sock.send(j)
      res = self.bank_sock.recv(BUFS)
      conn.send(res)
    #Deposit
    elif _type == 'd':
      d = self.check_deposit_threshold(amt, stash, d)
      j = json.dumps(d)  # make new dict
      self.bank_sock.send(j)
      res = self.bank_sock.recv(BUFS)
      conn.send(res)
    #withdraw    
    elif _type == 'w':
      d = self.check_withdraw_threshold(amt, stash, d)
      j = json.dumps(d)  # make new dict
      self.bank_sock.send(j)
      res = self.bank_sock.recv(BUFS)
      conn.send(res)
    else:
      print "wtf is this"

  def check_deposit_threshold(self, amt, stash, d):
    ''' checks thresholds after amt processed
        returns the altered dict with an added key value 
        is NOT null when transfer needs to be made '''
    upper = amt + stash 
    print "atm has {} available with a deposit request of {}".format(stash, amt)
    if (upper < self.high_thresh):
      d['putCash'] = None
      new_stash = stash + amt
      change_balance(self.atmID, new_stash)
    else:
      bank_forward = (amt + stash) - self.average_stash
      d['putCash'] = bank_forward 
      new_stash = (amt + stash) - bank_forward
      change_balance(self.atmID, new_stash)
      print "requesting {} be sent back to bank".format(bank_forward)
    print "ATM now has a stash of :: {}".format(self.get_available_cash())
    return d

  def check_withdraw_threshold(self, amt, stash, d):
    ''' same as above, but getCash set if the bank needs to send money 
    '''
    lower = stash - self.low_thresh
    print "atm has {} available with a withdrawal request of {}".format(stash, amt)
    if (lower > amt):
      d['getCash'] = None 
      new_stash = stash - amt
      change_balance(self.atmID, new_stash)
    # request cash from bank  
    else:
      bank_forward = amt + stash - self.average_stash
      d['getCash'] = bank_forward 
      new_stash = stash + bank_forward
      change_balance(self.atmID, new_stash)
      print "requesting {} from bank to cover withdraw".format(bank_forward)
    print "ATM now has a stash of :: {}".format(self.get_available_cash())
    return d             

  def get_available_cash(self):
    return float(get_atm_info(self.atmID)[4])
  
  def attempt_bank_conn(self):
    count = 0
    success = False
    while count < 10:
      # assume bank sockets are always on 8000
      try: 
        self.bank_sock.connect((self.bank_name, 8000))
        success = True
        break
      except socket.error: 
        print "trying to connect bank server @ {}:8000".format(self.bank_name) 
        time.sleep(5)
        count+=1
    if not success: 
      sys.exit("couldn't establish connection with bank host @ {}". format(self.bank_name))    
  def shutdown(self):
    print 'Shutting down --{}-- server...'.format(self.atmID)
    # Close sockets
    for o in self.readlist:
        o.close()
        
    self.atm_sock.close()  
    self.bank_sock.close()

if __name__ == '__main__':
  import sys
  if len(sys.argv) > 1 and sys.argv[1].startswith('--l'):
    sys.exit( [atm[0:3] for atm in list_atms('bank.db')] )
  elif len(sys.argv) < 3:
    sys.exit( Atm.__init__.__doc__ )
  Atm(sys.argv[1], sys.argv[2]).serve()

