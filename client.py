#!/usr/bin/env python

# ====CLIENT====
# setup:
# client atmID 
#
import os, sys
import socket
import getpass
import simplejson as json
from atm_utils import get_atm_info

BUFS = 4092


# set socket to blocking with a 30 second timeout

class Client():
  ''' client class for querying bank db '''

  def __init__(self, atmID):
    ''' 
    usage: ./client atmID \n\tatm id info is in the atm_list file
    '''
    
    self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.atmID = atmID
    atm_info = get_atm_info(self.atmID)
    atm_host = atm_info[1]
    atm_port = int(atm_info[2])
    try:
      self.s.connect(('localhost', atm_port)) 
      print "~~~Connection to {}:{} was successful~~~\n".format(atm_host, atm_port)
    except:
      sys.exit("no atm available at {}:{}".format( atm_host, atm_port ))

  def prompt(self): 
    #local copy of the socket
    s = self.s
    account = raw_input("Enter your account number: ")
    passwd  = getpass.getpass("Enter your Password: ")
    try: # keyboard interrupt exception
      q = self.query(_type='v',acc=account,passwd=passwd)
      s.send(q)
      res = s.recv(BUFS)
      print res
      if res == "validated":
        pass
      else:
        print "incorrect credentials, try again.."
        self.prompt()
      choice = 0

      while choice != 'x':
        choice = raw_input("enter one of the following options:\n"
                   "1 - check balance \n"
                   "2 - withdraw cash \n"
                   "3 - deposit cash \n"
                   "x - exit \n"
                   ">> ")
        
        if choice == '1':
          query = self.query(_type='cb',acc=account)
          s.sendall( query )
          print "~~~~~~~~~~~~~~"
          self.atm_recv(s)
          print "~~~~~~~~~~~~~~"
        if choice == '2':
          amt = raw_input("How much would you like to withdraw? \n>> ")
          query = self.query(_type='w',acc=account,amt=amt)
          s.sendall( query )
          print "~~~~~~~~~~~~~~"
          self.atm_recv(s)
          print "~~~~~~~~~~~~~~"
        if choice == '3':
          amt = raw_input("How much would you like to deposit? \n>> ")
          query = self.query(_type='d',acc=account,amt=amt)
          s.sendall( query )
          print "~~~~~~~~~~~~~~"
          self.atm_recv(s)
          print "~~~~~~~~~~~~~~"
      self.logout()  

    except KeyboardInterrupt:
      print "interrupted"
      self.logout()
    except socket.error:
      print "lost connection with atm"
      self.logout()
  
  def atm_recv(self, sock):
    ''' receive and parse the json from the atm response '''
    res = sock.recv(BUFS)
    if res:
      print json.loads(res)['res']
    else:
      print "lost connection to atm"

  def logout(self): 
    print "logging out"
    query = self.query(_type="x", acc=None)
    self.s.sendall( query )
    self.s.close()
    sys.exit(0)

  def query(self, _type, acc=None, passwd=None, amt=None, _from=None, to=None,
            res=None ):
    ''' json to hold type, acc, passwd, amt, and w/e else is required 
        v  - validate, 
        cb - check balance,
        d  - deposit
        w  - withdrawal
        x  - close '''
    d = {'type' :_type, 
        'acc'   :acc, 
        'passwd':passwd, 
        'amt'   :amt, 
        'from'  :_from, 
        'to'    :to, 
        'res'   :res }
    j = json.dumps(d)
    return j 

if __name__ == '__main__':
  import sys
  
  if len(sys.argv) < 2:
    sys.exit('{}'.format(Client.__init__.__doc__))
  Client(sys.argv[1]).prompt()

