import unittest
from bank import BankServer

class TestBankFunctions(unittest.TestCase):
    
  with open('test.db', 'w') as f:
    f.write('000 pass 4321\n')
  b = BankServer('test.db')
  print "init"

  def test_balance_rewrite(self):
    x = self.b.get_balance('000')
    self.b.acc_rewrite_balance('000', 1234)
    y = self.b.get_balance('000')
    self.assertNotEqual(x, y)
 
if __name__ == '__main__':
  unittest.main()
