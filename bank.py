#!/usr/bin/env python

# ====BANK====
# Bank takes [file_of_account] as an argument which is just: 
# --- account_number, password, initial_deposit ---stored in rows
#
# listen socket on port 8000
#

import os
import socket
import select
import locale
import simplejson as json
from db_utils import * 
from atm_utils import print_available_atms

PORT = 8000
BUFS = 4092
INITIAL_BANK_AMT = 100000000  # 100million dollar initial bank amount
MAX_ATMS = 2

class BankServer(object):
  ''' class for bank server '''
  
  def __init__(self, db_name="bank.db"):
    '''
    usage: ./bank bank.db 
    \t@param bank.db - db file to use/create filled with bank user info
    \ttype \'quit\' to stop serving
    '''

    self.db_name = db_name
    init_db(db_name)
    self.init_balance()
    # socket setup on port 8000
    self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.server.bind(('', PORT)) # bind all on port
    self.server.listen(MAX_ATMS)
    print self.parse_admin_input.__doc__
        
  def serve(self):
    self.readlist =  [ self.server, sys.stdin ]

    print "waiting for connections/requests from atms..."
    quit = False
    while not quit:
      self.sread, swrite, sexc = select.select(self.readlist, [], [])
      for s in self.sread:
        if s == self.server:
          conn, addr = s.accept()
          print "connected to atm @ ", addr
          conn.setblocking(0)
          self.readlist.append(conn)
        elif s == sys.stdin:
          admin_input = sys.stdin.readline()
          if admin_input.startswith('q'):
            quit = True
            break
          else: self.parse_admin_input(admin_input)
        # not in sread or stdin
        else:
          req = s.recv(BUFS)
          try:
            print "atm req: ".format(req)
            if req:
              self.parse_atm_req(req, s)
            else:
              print "atm disconnected"
              if s in self.readlist:
                self.readlist.remove(s)
                s.close()
          except socket.error:
            print "error with atm, disconnected"
            self.readlist.remove(s)
    self.shutdown()
 
  def parse_atm_req(self, req, conn):
    d = json.loads(req)
    _type   = d['type']
    acc     = d['acc']
    passwd  = d['passwd']
    amt     = d['amt']
    # Validate
    if _type == "v":
      if check_creds(acc, passwd):
        print "valid password for account {}".format(acc)
        conn.send("validated {}".format(acc))
      else: 
        print "user: {} and pass: {} were invalid".format(acc,passwd)
        conn.send("!valid")
    # check balance    
    elif _type == 'cb':
      res = self.get_balance(acc)
      d['res'] = "your current balance is: " + format_currency(res)
      j = json.dumps(d)
      conn.send(j)
    # Deposit    
    elif _type == 'd':
      new_balance = self.acc_deposit(acc, amt) 
      if new_balance:
        d['res'] = "your new balance is: " + format_currency(new_balance)
        j = json.dumps(d)
        conn.send(j)
      else:
        print "problem with deposit"
    # Withdrawal    
    elif _type == 'w':
      atm_forward = d['getCash']
      if atm_forward:
        new_balance = self.get_balance() - atm_forward
        new_bank_balance(new_balance) 
      new_balance = self.acc_withdraw(acc, amt)
      if new_balance:
        d['res'] = "your new balance is: " + format_currency(new_balance)
        j = json.dumps(d)
        conn.send(j)
      else:
        d['res'] = "insufficient funds "
        j = json.dumps(d)
        conn.send(j)

    else:
      print "wtf is this"

  def shutdown(self):
    print 'Shutting down --{}-- server...'.format(self.db_name)
    # Close sockets
    for c in self.readlist:
      c.close()
    self.server.close() 

  def account_available(self, acc):
    if (self.get_account_info(acc_num) == ''): return True
    else: return False  
    
  def get_account_info(self, acc_num):
    ''' returns the empty string if no account exists '''
    with open(self.db_name, 'r+') as f:
      for line in f.readlines():
        if line.startswith(acc_num):
          return line
        else: pass 
      return ""

  def create_account(self, acc_num, passwd, initial_deposit):
    with open(self.db_name, 'a') as f:
      print "wrote account {} to {} with an initial deposit of {}".format( 
              acc_num, self.db_name, format_currency(initial_deposit) )
      f.write("{} {} {}\n".format(acc_num, passwd, initial_deposit))

  def check_passwd(self, acc_num, passwd):
    print "checking passwd for account {}".format(acc_num)
    info_string = self.get_account_info(acc_num)
    info = info_string.split(' ')
    if (info[0] == acc_num) and (info[1] == passwd):
      print "password success"
      return True
    else: return False

  def get_balance(self, acc_num=None):
    ''' get bank balance or account balance depending on 
        @param acc_num  - if none, checks banks balance'''
    if acc_num:
      print "getting balance for client"
      info_string = self.get_account_info(acc_num)
      info = info_string.split(' ')
      print 'balance is {}'.format(info[2])
      return float(info[2])
    else:  
      path =  os.path.join(os.getcwd(), 'bank_balance')
      with open(path, 'r') as f:
        c = f.readline()
      return float(c)

  def acc_withdraw(self, acc_num, amount):
    ''' returns True if success, else false '''
    cur_balance = self.get_balance(acc_num)
    new_balance = cur_balance - float(amount)
    if (new_balance > 0):
      self.acc_rewrite_balance(acc_num, new_balance)
      return new_balance
    else: 
      return False

  def acc_deposit(self, acc_num, amount):
    cur_balance = self.get_balance(acc_num)
    new_balance = cur_balance + float(amount)
    self.acc_rewrite_balance(acc_num, new_balance)
    return new_balance

  def init_balance(self):
    path =  os.path.join(os.getcwd(), 'bank_balance')
    if os.path.exists(path):
      pass
    else:
      with open(path, 'w') as f:
        f.write(str(INITIAL_BANK_AMT))
  
  def acc_rewrite_balance(self, acc_num, new_balance):
    #TODO needs documentation
    with open(self.db_name, "r") as f:
      data = f.readlines()
    for acc in data: 
      if acc.startswith(str(acc_num)):
        entry = acc
    entry_list = entry.split(' ')
    entry_list[2] = str(new_balance) + "\n"
    new_entry = ' '.join(entry_list)
    data[data.index(entry)] = new_entry
    with open(self.db_name, "w") as f:
      f.writelines( data )
  
  def new_account_prompt(self):
    quit = False
    while not quit:
      acc_num       = raw_input("What is the account number? \n>> ")
      passwd        = raw_input("What is the password? \n>> ")
      init_deposit  = raw_input("How much would you like initially deposited? \n>> ")
    
      print 'checking account availablility for {}'.format(acc_num)
      if self.get_account_info( acc_num ) == '':
        self.create_account( acc_num, passwd, init_deposit )
        quit = True
      else:
        print "account already exists"
        if raw_input("try again? (y/n) ") == 'y':
          self.new_account_prompt()
        else:
          quit = True
          break
    print "~~returning to main bank admin menu~~"
          
  def parse_admin_input(self, ain):
    ''' 
    admin input options:
    list_atms   - print out available atms and their info
    balance     - print out THIS bank's available funds
    addAccount  - add a new client\'s account details
    registerATM - not implemented yet
    help        - print out these input options
    quit        - ...quit
    '''
    # list connected atms
    if ain.startswith( 'list_atms' ):
      print "-----------\n{}----------".format(print_available_atms())
    elif ain.startswith( 'balance' ):
      print "----------\n{}\n----------".format(format_currency(self.get_balance()))
    elif ain.startswith( 'addAccount' ):
      self.new_account_prompt()
    elif ain.startswith('registerATM' ):
      pass
      #new_atm_registration_prompt()
    elif ain.startswith( 'help' ):
      print self.parse_admin_input.__doc__


if __name__ == '__main__':
  import sys
  
  if len(sys.argv) < 2:
    sys.exit(BankServer.__init__.__doc__)
  BankServer(sys.argv[1]).serve()

