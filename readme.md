# ATM client simulation
## Brady Ouren    

client:  ./client [atmID]
=======

@param atmID - specify which atm you are connecting to

The client interface asks for a username (3 digit number) and a valid password
before entering the prompt for requests.  This validates the user with the
banks database and allows the user to continue only if they have valid
credentials to avoid passing the clients information around on every request

##functionality:
* check balance 
* deposit 
* withdraw 

atm:  ./atm [atmID] [bank_name]
====
@param atmID     - specify the atm identification name (3 digit alphanumeric)
@param bank_name - the HOSTNAME of bank / defaults a bind on port 8000
Type ./atm.py --list to list the available atm IDs

the atm info is kept in a file called atm_list which holds a record:
atmID || host || port || bank_associated_with || amount_held
in each row

* Receives connections from client and passes info on to the bank it is
associated with.
* There is a Low Threshold and High Threshold which represent the mimimum of cash
which should be kept at hand
* each atm starts with a default of $100,000.00

bank:  ./bank [file_of_account]
=====

@param bank.db - space delimited list for each account at this bank

## bank.db
holds:
account_number1 passwd balance1
account_number2 passwd balance2
...etc
- each BankServer instance is associated with the banks specific db, or
  [file_of_account]

## Initial Amount
defaults to $100,000,000.00

### distributed files
the atm_utils should be accessible to atm and client and db_utils to bank.
Obviously bank, bank_balance, and bank.db should be together as well
