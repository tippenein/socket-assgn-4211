import os
import locale

locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

def format_currency(cur):
  ''' Must convert to float since the input is a string 
      Otherwise you're just returning a $100,000.00 string'''
  if type( cur ) == 'float': return locale.currency(cur, grouping=True)
  else: return locale.currency(float(cur), grouping=True)


def init_db(db_name, typ='bank_accounts'):
  ''' initialize any type of new db'''
  if '/' in db_name: 
    raise NameError('No / characters allowed in db_name')
  cwd = os.getcwd()
  file_path = os.path.join(cwd, db_name)
  if os.path.exists(file_path):
    pass
  else:    
    with open(file_path, 'w') as f:
      f.write("----------{}---------\n".format(typ))

def new_bank_balance(amt):
  with open('bank_balance', "w") as f:
    f.write( str(amt) )

def check_creds(acc, passwd, bank='bank.db'):
  if acc:
    with open(bank, 'r') as f:
      for line in f.readlines():
        if line.startswith(acc):
          if (line.split(' ')[1] == passwd):
            return True
          else: return False
  else:
    return False
  
