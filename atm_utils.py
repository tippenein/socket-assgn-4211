import os

def list_atms(bank):
  ''' returns a list of atms associated with param @bank
      format is atmID host port bank'''
  atm_list = []
  with open('atm_list', 'r') as f:
    for line in f.readlines():
      if line.startswith('#'):
        pass
      else:
        if bank in line:
          atm_list.append(line)
  return atm_list

def change_balance(atmID, amt):
  amt = float(amt)
  with open('atm_list', "r") as f:
    data = f.readlines()
  for atm in data: 
    if atm.startswith(atmID):
      entry = atm
  entry_list = entry.split(' ')
  entry_list[4] =  str(amt) + "\n"
  new_entry = ' '.join(entry_list)
  data[data.index(entry)] = new_entry
  with open('atm_list', "w") as f:
    f.writelines( data )

def print_available_atms(bank='bank.db'):
  lis = list_atms(bank)
  e = ''
  for item in lis:
    e += item.split(' ')[0] + "\n"
  return e

def get_atm_info(atmID):
  with open('atm_list') as f:
    for line in f.readlines():
      if line.startswith(atmID):
        return line.split(' ')

def get_atm_tuple(atmID):
  q = get_atm_info(atmID)
  return q[1], q[2]

def write_atm_info(atmID, host, port, bank, amt):
  entry = ' '.join(atmID, str(host), str(port), bank, str(amt))
  with open('atm_list', 'a') as f:
    f.writeline(entry)    
